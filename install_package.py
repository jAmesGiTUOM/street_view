# installing the required package
def install_package():
    import pip
    required_module = [ "django", "pillow","psycopg2","requests"]
    # pip version above 10.0.0 won't support pip.main
    pip.main(["install","--upgrade","pip==9.0.1"])
    for module in required_module:
        pip.main(["install",module])


if __name__ == "__main__":
    install_package()