import io

from django.http import HttpResponse

from helper_packages.image_retrieve import google_pic_access_point


# Create your views here.


def get_panorama(request):
    lat = request.GET.get('lat', '')
    lng = request.GET.get('lng', '')
    image_object = google_pic_access_point.get_streetview_picture(lat, lng)
    binary_data = io.BytesIO()
    image_object.save(binary_data, format='PNG')
    binary_data = binary_data.getvalue()

    response = HttpResponse(binary_data, content_type="image/png")
    response['Content-Disposition'] = 'attachment; filename=test.png'

    return response
