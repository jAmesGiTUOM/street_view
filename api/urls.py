from django.urls import path

from . import views

urlpatterns = [
    path('panorama', views.get_panorama, name='panorama_api')
]