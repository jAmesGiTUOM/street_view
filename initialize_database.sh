#!/usr/bin/env bash
# install system packages
echo "installing required software"
sudo apt-get update
sudo apt-get install postgresql
# let the user set the password
echo "Enter a database user name"
read USERNAME
echo "Enter a password for that user"
read -s PASSWORD
echo "Enter a name for the database"
read DATABASE

#create db
sudo -u postgres createdb $DATABASE
# create the user
sudo -u postgres createuser $USERNAME
sudo -u postgres psql -c "ALTER USER $USERNAME PASSWORD '$PASSWORD';"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE $DATABASE TO $USERNAME;"

# save the password to a file
sudo echo "localhost:5432:$DATABASE:$USERNAME:$PASSWORD" >> ~/.pgpass
sudo chmod 0600 ~/.pgpass

# create table
psql -h localhost -U $USERNAME $DATABASE -c "CREATE TABLE grid (
     x_coordinate NUMERIC NOT NULL,
     y_coordinate NUMERIC NOT NULL,
     has_ORI_image BOOL,
     has_SAT_image BOOL,
     has_SKY_image BOOL,
     PRIMARY KEY(x_coordinate,y_coordinate)
);"

psql -h localhost -U $USERNAME $DATABASE -c "CREATE TABLE image (
     image_id SERIAL NOT NULL,
     image_data BYTEA,
     grid_breakdown TEXT,
     total_breakdown TEXT,
     SVF NUMERIC,
     x NUMERIC NOT NULL,
     y NUMERIC NOT NULL,
     image_type TEXT NOT NULL,
     created_at TIMESTAMP DEFAULT NOW(),
     PRIMARY KEY(image_id),
     FOREIGN KEY (x,y) REFERENCES grid (x_coordinate,y_coordinate)
);"

#set up the conn string file
echo "dbname='$DATABASE' user='$USERNAME' host='localhost'" > conn_string.txt
