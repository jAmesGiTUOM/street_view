"""
Author: Jingcheng Wang
This is a python implementation of the algorithm describes in
https://stackoverflow.com/questions/34250742/converting-a-cubemap-into-equirectangular-panorama
It stitches 6 images into a cubic map, then folds the cubic map to a panorama image.
"""
from PIL import Image
import math



"""
this is the access point of the package. 
input: a list of picture files(either as static files or io.BytesIO object) + the output dimension
output: a pillow image object, which is the panoramic image.
"""


def generate_panorama_from_six_images(image_list, output_width, output_length):
    cubic_image = stitch_pics_to_cubic_map(image_list)
    cubic_image_source = cubic_image.load()
    result_data = convert_cubic_to_panorama(cubic_image_source, output_width, output_length)
    result_image = Image.new(cubic_image.mode, (output_width, output_length))
    result_image.putdata(result_data)
    return result_image


"""
Take 6 image files as input then stitching them into a cubic map
"""


def stitch_pics_to_cubic_map(picture_path_list):
    image_list = []
    image_data_list = []
    n = 0
    for picture_path in picture_path_list:
        # picture_path variable can be either a io.BytesIO() object or a static file path
        image_list.append(Image.open(picture_path))
        image_data_list.append(image_list[n].load())
        n += 1

    length = image_list[0].size[1]
    width = image_list[0].size[0]
    # a cubic map has 4 images horizontally, 3 images vertically.
    result_image = Image.new(image_list[0].mode, (width*4, length*3))
    pixel_data = combine_pixel(image_data_list, width, length)
    result_image.putdata(pixel_data)
    return result_image

"""
project each pixel from the 6 individual images onto the cubic map
"""


def combine_pixel(pixel_data_list, width, length):
    resultlist = []
    for y in range(0, length):
        for n in range(0, 4):
            for x in range(0, width):
                if n == 1:
                    resultlist.append(pixel_data_list[0][x, y])
                else:
                    resultlist.append((0, 0, 0, 0))

    for y in range(0,length):
        for n in range(1, len(pixel_data_list)-1):
            for x in range(0,width):
                resultlist.append(pixel_data_list[n][x, y])

    for y in range(0, length):
        for n in range(0, 4):
            for x in range(0, width):
                if n == 1:
                    resultlist.append(pixel_data_list[-1][x, y])
                else:
                    resultlist.append((0, 0, 0, 0))

    return resultlist


"""
the function will take a source image data(2D array of RGBA value),and the output dimension as input
and output a 1D array of RGBA value of the panoramic image
the explanation of the algorithm can be seen at 
https://stackoverflow.com/questions/34250742/converting-a-cubemap-into-equirectangular-panorama
"""


def convert_cubic_to_panorama(source_texture, output_width, output_length):
    cube_width = 640
    cube_height = 640
    resultlist = []
    for j in range(0,output_length):
        v = 1 - j/output_length
        theta = v * math.pi

        for i in range(0,output_width):
            u = i/output_width
            phi = u*2*math.pi

            x = math.sin(phi) * math.sin(theta) * -1
            y = math.cos(theta)
            z = math.cos(phi) * math.sin(theta) * -1

            a = max([abs(x), abs(y), abs(z)])

            xa = x / a
            ya = y / a
            za = z / a

            if xa == 1:
                xPixel = int((((za + 1)/2) - 1)*cube_width)
                xOffset = 2 * cube_width
                yPixel = int(((ya+1)/2)*cube_height)
                yOffset = cube_height
            elif xa == -1:
                xPixel = int(((za + 1) / 2) * cube_width)
                xOffset = 0
                yPixel = int(((ya + 1) / 2) * cube_height)
                yOffset = cube_height
            elif ya == 1:
                xPixel = int(((xa + 1) / 2) * cube_width)
                xOffset = cube_width
                yPixel = int((((za + 1) / 2) - 1) * cube_height)
                yOffset = cube_height * 2
            elif ya == -1:
                xPixel = int(((((xa + 1) / 2)) * cube_width))
                xOffset = cube_width
                yPixel = int(((((za + 1) / 2)) * cube_height))
                yOffset = 0
            elif za == 1:
                xPixel = int(((((xa + 1) / 2)) * cube_width))
                xOffset = cube_width
                yPixel = int(((((ya + 1) / 2)) * cube_height))
                yOffset = cube_height
            elif za == -1:
                xPixel = int(((((xa + 1) / 2) - 1) * cube_width))
                xOffset = 3 * cube_width
                yPixel = int(((ya + 1) / 2) * cube_height)
                yOffset = cube_height
            else:
                raise Exception("Something is wrong")

            xPixel = abs(xPixel)
            yPixel = abs(yPixel)

            xPixel += xOffset
            yPixel += yOffset
            try:
                resultlist.append(source_texture[xPixel, yPixel])
            except:
                resultlist.append(source_texture[xPixel-1, yPixel-1])

    return resultlist

