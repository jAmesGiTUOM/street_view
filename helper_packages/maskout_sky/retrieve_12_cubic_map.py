import requests
import io
from PIL import Image
apikey = 'AIzaSyC2aeVY254H85iFXw0BywSAS18VwvnGRoc'

"""
get 12 streetview picture for a picture, stitch them, then return the image object
"""
def get_streetview_picture(lat,lng):
    lat = str(lat)
    lng = str(lng)
    metadata = get_streetview_metadata(lat, lng)
    status = metadata['status']
    # check if google api has image for that location,then retrieve and stitch those images
    if status != 'OK':
        print("No streetview here!")
        return
    url = 'https://maps.googleapis.com/maps/api/streetview'
    location = lat + ',' + lng
    picture_list = []
    # get the 4 horizontal pics
    for pitch in [90,0,-90]:
        for angle in [0,90,180,270]:
            param = {'location': location, 'key': apikey,'size':'640x640','heading':angle,'pitch':pitch,'source':'outdoor'}
            picture_byte = requests.get(url,param).content
            picture_list.append(io.BytesIO(picture_byte))

    # perform the stiching process
    result_image = stitch_pics_to_cubic_map(picture_list)

    return result_image


"""
get the metadata for a location, to check if there are actually street view pictures at that location
"""
def get_streetview_metadata(lat,lng):
    url = 'https://maps.googleapis.com/maps/api/streetview/metadata'
    location = lat+','+lng
    param = {'location':location,'key':apikey}
    result = requests.get(url,param).json()
    return result

"""
Stitch the 12 seperate images into a cubic map
"""
def stitch_pics_to_cubic_map(picture_path_list):
    image_list = []
    image_data_list = []
    n = 0
    for picture_path in picture_path_list:
        image_list.append(Image.open(picture_path))
        image_data_list.append(image_list[n].load())
        n+=1

    length = image_list[0].size[1]
    result_image = Image.new(image_list[0].mode, (640*4, length*3))
    pixel_data = combine_pixel(image_data_list,image_list[0].size[0],image_list[0].size[1])
    result_image.putdata(pixel_data)
    #return draw_lines_to_segment_image(result_image)
    return result_image

# helper function for stitch_pics_to_cubic_map
def combine_pixel(pixel_data_list, width, length):
    resultlist = []
    for y in range(0, length):
        for n in range(0, 4):
            for x in range(0, width):
                resultlist.append(pixel_data_list[n][x, y])

    for y in range(0,length):
        for n in range(4, 8):
            for x in range(0,width):
                resultlist.append(pixel_data_list[n][x, y])

    for y in range(0, length):
        for n in range(8, 12):
            for x in range(0, width):
                resultlist.append(pixel_data_list[n][x, y])

    return resultlist
