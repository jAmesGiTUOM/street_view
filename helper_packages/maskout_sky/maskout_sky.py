import pymeanshift as pms
import numpy as np
from PIL import Image
import sys

"""
mask out the sky for a 12 cubic image,return the result image as a PIL image obj 
"""
def maskout_sky(input_image):
    RED = 0
    GREEN = 1
    BLUE = 2
    data = np.asarray(input_image,dtype="int32")
    (segmented_image, labels_image, number_regions) = pms.segment(np.asarray(input_image), spatial_radius=6,
                                                                      range_radius=7, min_density=40)
    new_labels_image = mask_out_sky(labels_image)
    for i in range(0, input_image.size[1]):
        for j in range(0, input_image.size[0]):
            if new_labels_image[i][j] == -1:
                data[i, j, GREEN] = 0
                data[i, j, RED] = 0
                data[i, j, BLUE] = 255
            else:
                continue
    result = Image.fromarray(data.astype(np.uint8))
    ms = Image.fromarray(segmented_image)
    return result

def mask_out_sky(labels_image):
    # find the most common colour
    first_region = labels_image[0:960,0:640]
    second_region = labels_image[0:960,640:1280]
    third_region = labels_image[0:960,1280:1920]
    forth_region = labels_image[0:960,1920:2560]

    backtrack = True
    times_backtrack = 0
    while backtrack:
        backtrack = False
        most_common_colour = find_most_common_colour(first_region,times_backtrack)#np.bincount(first_region[:, 0]).argmax()
        for j in range(0,640):
            if backtrack:
                break
            for i in range (0,1280):
                if labels_image[i][j] == most_common_colour:
                    if i >960:
                        backtrack = True
                        times_backtrack += 1
                        break
                    else:
                        labels_image[i][j] = -1

    backtrack = True
    times_backtrack = 0
    while backtrack:
        backtrack = False
        most_common_colour = find_most_common_colour(second_region,times_backtrack)#np.bincount(first_region[:, 0]).argmax()
        for j in range(640,1280):
            if backtrack:
                break
            for i in range (0,1280):
                if labels_image[i][j] == most_common_colour:
                    if i >960:
                        backtrack = True
                        times_backtrack += 1
                        break
                    else:
                        labels_image[i][j] = -1

    backtrack = True
    times_backtrack = 0
    while backtrack:
        backtrack = False
        most_common_colour = find_most_common_colour(third_region,times_backtrack)#np.bincount(first_region[:, 0]).argmax()
        for j in range(1280,1920):
            if backtrack:
                break
            for i in range (0,1280):
                if labels_image[i][j] == most_common_colour:
                    if i >960:
                        backtrack = True
                        times_backtrack += 1
                        labels_image[0:960,1280:1920] = third_region
                        break
                    else:
                        labels_image[i][j] = -1

    backtrack = True
    times_backtrack = 0
    while backtrack:
        backtrack = False
        most_common_colour = find_most_common_colour(forth_region,times_backtrack)#np.bincount(first_region[:, 0]).argmax()
        for j in range(1920,2560):
            if backtrack:
                break
            for i in range (0,1280):
                if labels_image[i][j] == most_common_colour:
                    if i >960:
                        backtrack = True
                        times_backtrack += 1
                        break
                    else:
                        labels_image[i][j] = -1

    return labels_image


def find_most_common_colour(label_image,times_backtrack):
    d = {}
    for j in range(0,640):
        for i in range(0,960):
            if label_image[i,j] in d:
                d[label_image[i,j]] +=1
            else:
                d[label_image[i,j]] = 0

    while times_backtrack >=0:
        temp_key = max(d,key=d.get)
        if times_backtrack == 0:
            return temp_key
        else:
            del d[temp_key]
            times_backtrack -= 1

    return max(d,key=d.get)
