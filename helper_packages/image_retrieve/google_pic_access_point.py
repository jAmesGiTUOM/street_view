import io

import requests

from helper_packages.image_stiching import stitching_tools

apikey = 'AIzaSyC2aeVY254H85iFXw0BywSAS18VwvnGRoc'

"""
get 6 streetview picture for a picture, stitch them, then save the picture to the picture_path
"""
def get_streetview_picture(lat,lng):
    url = 'https://maps.googleapis.com/maps/api/streetview'
    location = lat + ',' + lng
    picture_list = []

    # get the upward pic
    param = {'location': location, 'key': apikey, 'size': '640x640', 'heading': 90,'pitch':90}
    picture_byte = requests.get(url, param).content
    picture_list.append(io.BytesIO(picture_byte))

    # get the 4 horizontal pics
    for angle in [0, 90, 180, 270]:
        param = {'location': location, 'key': apikey,'size':'640x640','heading':angle}
        picture_byte = requests.get(url,param).content
        picture_list.append(io.BytesIO(picture_byte))

    # get the downward pic
    param = {'location': location, 'key': apikey, 'size': '640x640', 'heading': 90, 'pitch': -90}
    picture_byte = requests.get(url, param).content
    picture_list.append(io.BytesIO(picture_byte))
    # perform the stiching process, and save the picture to the location
    # 1280,960 is the current output resolution we are using.
    result_image = stitching_tools.generate_panorama_from_six_images(picture_list, 1280, 960)
    return result_image


"""
get the metadata for a location, to check if there are actually street view pictures at that location
"""
def get_streetview_metadata(lat,lng):
    url = 'https://maps.googleapis.com/maps/api/streetview/metadata'
    location = lat+','+lng
    param = {'location':location,'key':apikey}
    result = requests.get(url,param).json()
    return result
