# README #

# you can get panorama images using the web API

example: http://13.55.67.216:8000/api/panorama?lat=-37.800398&lng=144.963957((no longer available)
)

### What is this repository for? ###
The repo is the source code for Computing Project by Jingcheng Wang

### How do I get started? ###

There is a live website running on AWS @ http://13.55.67.216:8000/
Copy the address into a browser and you can start use the web application(no longer available)

### Dependencies
     django
     pillow
     psycopg2
     requests

# Deployment instructions

The initialize.sh is a bash script for automating the deployment process
It's design to run on a ubuntu 16.04 system. There is just one step:

1.run "./initialize.sh" (don't use "sh initialize.sh").

It will install all the required packages, setup the database, and start the server as a background task.

\*
If you are running a ubuntu version lower than 16.04 or maybe other ubuntu/debian distros. The script will not work.
In that case, you can run the initialize_database.sh to setup the database, and then manually install all the required python packages in you python environment
, and start the server. So the steps will be:

1. run initialize_database.sh using "./initialize_database.sh"

2. follow the prompt to enter a new user name/password/database name to create a new database

3. install all the packages listed in dependencies using pip

4. go to the project root folder and run "python manage.py runserver 0.0.0.0:8000"

\* if you encounter any database connection problems, you can either manually modify the
conn_string.txt file with the credential details set up previously, or setup an new account and an new database.
 