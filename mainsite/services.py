"""
The service component(layer) in the M-S-V-T architecture
performs all the complicated computational operations for the view layer
"""
import operator
from decimal import Decimal

from PIL import Image
from django.http import HttpResponseRedirect

from helper_packages.image_retrieve import google_pic_access_point as image_retrieval
from helper_packages.maskout_sky import maskout_sky,retrieve_12_cubic_map
from .models.grid import Grid
from .models.image import Image as Image_model

# the static rgba value for different types of pixels
EMPTY_PIXEL = (0, 0, 0, 0)
RED_PIXEL = (255, 0, 0, 255)
GREEN_PIXEL = (0, 128, 0, 255)
BLUE_PIXEL = (0, 0, 255, 255)
BLACK_PIXEL = (0, 0, 0, 255)

def get_streetview_metadata(lat,lng):
    return image_retrieval.get_streetview_metadata(lat,lng)

def get_streetview_picture(lat,lng,picture_path):
    result_image = image_retrieval.get_streetview_picture(lat,lng)
    result_image.save(picture_path)
    # add the new image to the database, the type is "ORI"
    with open(picture_path, 'rb+') as f:
        picture_byte = f.read()
        Image_model.add_new_image(picture_byte, convert_to_decimal(lat),
                                  convert_to_decimal(lng), "ORI", None, None, None)
    return True

"""
a utility function that extends on HttpResponseRedirect by accepting kwargs and pass them into a GET request
"""
def custom_redirect(url_name, **kwargs):
    import urllib.parse
    params = urllib.parse.urlencode(kwargs)
    return HttpResponseRedirect(url_name + "?%s" % params)

"""
get the dimension of a particular image
"""
def get_image_size(picture_path):
    im = Image.open(picture_path)
    width, height = im.size
    return {'height':height,'width':width}

"""
calculates the SVF data for a cropped image
"""
def generate_SVF_data(picture_path, picture_data, lat, lng):
    with open(picture_path, 'wb+') as f:
        f.write(picture_data)
    image = Image.open(picture_path)
    pixels = image.load()
    empty_pixel_num = 0
    for x in range(0,image.size[0]):
        for y in range(0,image.size[1]):
            # check if it is empty pixel
            if is_certain_pixel(EMPTY_PIXEL, pixels[x, y]):
                empty_pixel_num += 1

    percentage = convert_to_decimal(empty_pixel_num*1.0/(image.size[0]*image.size[1]))
    # add the image data to the database
    Image_model.add_new_image(picture_data,convert_to_decimal(lat),
                              convert_to_decimal(lng), "SKY", None, None, percentage)
    return percentage

"""
calculates the satillite data for a marked image, but only the percentage comparing to the entire image
"""
def generate_SAT_data(picture_path,picture_data,colour_list):
    if not colour_list or len(colour_list) == 0:
        return None
    with open(picture_path, 'wb+') as f:
        f.write(picture_data)
    image = Image.open(picture_path)
    pixels = image.load()
    colour_pixel_dict = {}
    for colour in colour_list:
        colour_pixel_dict[colour] = 0
        for x in range(0, image.size[0]):
            for y in range(0, image.size[1]):
                if is_certain_pixel(colour, pixels[x, y]):
                    colour_pixel_dict[colour] += 1

    return translate_colour_dict_to_string(colour_pixel_dict, image.size[0], image.size[1])

"""
a simple function to improve readability
"""
def is_certain_pixel(sample,pixel):
    if sample == pixel:
        return True
    return False

"""
decode the image data passed from a POST request
"""
def process_client_image(image_data):
    import re
    import base64
    dataUrlPattern = re.compile('data:image/(png|jpeg);base64,(.*)$')
    dataPattern = dataUrlPattern.match(image_data)
    # check if the data is empty
    if dataPattern is None:
        return None
    result = dataPattern.group(2)

    # Decode the 64 bit string into 32 bit
    result = base64.b64decode(result)

    return result

def process_colour_list_string():
    # for now,just return red,green,blue
    return [RED_PIXEL, GREEN_PIXEL, BLUE_PIXEL]

"""
same logic as generate_sat_data,but do it for every grid on an image
"""
def generate_grid_data(picture_path,colour_list,grid_number):
    if not colour_list or len(colour_list) == 0:
        return None
    image = Image.open(picture_path)
    pixels = image.load()
    grid_size_x = image.size[0]/grid_number
    grid_size_y = image.size[1]/grid_number
    grid_dict = {}

    for x_grid in range(0,grid_number):
        for y_grid in range(0,grid_number):
            colour_dict = {}
            x_lower = int(x_grid*grid_size_x)
            x_upper = int((x_grid+1)*grid_size_x)
            y_lower = int(y_grid*grid_size_y)
            y_upper = int((y_grid+1)*grid_size_y)
            for colour in colour_list:
                colour_dict[colour] = 0
                for x_pixel in range(x_lower, x_upper):
                    for y_pixel in range(y_lower, y_upper):
                        if is_certain_pixel(colour, pixels[x_pixel, y_pixel]):
                            colour_dict[colour] += 1
            grid_dict[x_grid, y_grid] = translate_colour_dict_to_string(colour_dict, grid_size_x, grid_size_y)
    return sort_dict_to_list(grid_dict)

def translate_colour_dict_to_string(colour_pixel_dict, height, width):
    red = convert_to_decimal(colour_pixel_dict[RED_PIXEL]/(height*width))
    green = convert_to_decimal(colour_pixel_dict[GREEN_PIXEL]/(height*width))
    blue = convert_to_decimal(colour_pixel_dict[BLUE_PIXEL]/(height*width))

    return {'Building': red, 'Vegetation': green, 'Road': blue}


def sort_dict_to_list(input_dict):
    return sorted(input_dict.items(), key=operator.itemgetter(0), reverse=False)


def add_new_grid(lat,lng):
    x = convert_to_decimal(lat)
    y = convert_to_decimal(lng)
    return Grid.add_new_grid(x,y)


def load_old_grid(lat,lng):
    image_data_results = get_all_image(lat, lng, None)
    result_dict = {'ORI':0,'SAT':0,'SKY':0}
    for image in image_data_results:
        image_type = image['image_type']
        if image_type == "MOS_CUB" or image_type == "MOS_PANO":
            continue
        result_dict[image_type] += 1
        image_data = bytes(image['image_data'])
        picture_path = find_picture_path_by_type(image_type)
        with open(picture_path, 'wb+') as f:
            f.write(image_data)
    return result_dict

"""
change a float number to 3 digit decemal
"""
def convert_to_decimal(value):
    return Decimal(value).quantize(Decimal('0.001'))

"""
return the path to temporarily store a picture loaded from the database
"""
def find_picture_path_by_type(type):
    from .views import picture_path_SAT_original,picture_path_SVF_original,picture_path_SVF_cropped
    if type == "ORI":
        return picture_path_SVF_original
    elif type == "SAT":# not actually in use
        return picture_path_SAT_original
    elif type == "SKY":
        return picture_path_SVF_cropped
    else:
        return None

"""
get all the saved images at a particular location
"""
def get_all_image(lat, lng, image_type):
    return Image_model.get_all_image_data_at_a_location(convert_to_decimal(lat),convert_to_decimal(lng), image_type)

def find_an_image(id):
    return Image_model.get_image_data_by_id(id)

"""
update the local marked/cropped image file for displaying
"""
def load_image_as_file(image, type):
    from .views import picture_path_SVF_cropped,picture_path_SAT_cropped
    image_data = bytes(image['image_data'])
    if type == 'SVF':
        with open(picture_path_SVF_cropped, 'wb+') as f:
            f.write(image_data)
    else:
        with open(picture_path_SAT_cropped, 'wb+') as f:
            f.write(image_data)

"""
update the original image saved for a location in the database
"""
def update_image(image_data,lat,lng):
    Image_model.update_image_data_original(image_data,convert_to_decimal(lat),convert_to_decimal(lng))


"""
get a list of all the visited grids
"""
def get_all_grid():
    return Grid.get_all_grid()

"""
prepare the maskout_sky image
"""

def prepare_maskout_sky_image(lat,lng):
    from helper_packages.image_stiching import stitching_tools
    from .views import picture_path_MOS_pano,picture_path_MOS_cubic
    # finding data in the DB
    cubic_result_list = get_all_image(lat,lng,"MOS_CUB")
    if len(cubic_result_list) != 0:
        cubic_image = cubic_result_list[0]
        pano_image = get_all_image(lat,lng,"MOS_PANO")[0]
        print(bytes(pano_image['image_data']))
        with open(picture_path_MOS_cubic,"wb+") as f:
            f.write(bytes(cubic_image['image_data']))
        with open(picture_path_MOS_pano, "wb+") as f:
            f.write(bytes(pano_image['image_data']))
        return

    # data not present in the DB, process a new one
    # get 12-cubic image from google API
    cubic_image = retrieve_12_cubic_map.get_streetview_picture(lat,lng)
    # maskout the sky on the 12-cubic image and save it locally
    result_image = maskout_sky.maskout_sky(cubic_image)
    result_image.save(picture_path_MOS_cubic)
    # add the image to the database
    with open(picture_path_MOS_cubic,"rb+") as f:
        picture_byte = f.read()
        Image_model.add_new_image(picture_byte, convert_to_decimal(lat),
                                  convert_to_decimal(lng), "MOS_CUB", None, None, None)

    # stitch the cubic image back to panorama
    pano_image_data = stitching_tools.convert_cubic_to_panorama(result_image.load(),1280,960)
    pano_image = Image.new(result_image.mode,(1280,960))
    pano_image.putdata(pano_image_data)
    pano_image.save(picture_path_MOS_pano)
    with open(picture_path_MOS_pano,"rb+") as f:
        picture_byte = f.read()
        Image_model.add_new_image(picture_byte, convert_to_decimal(lat),
                                  convert_to_decimal(lng), "MOS_PANO", None, None, None)


