"""
Author: Jingcheng Wang
The view in the Django Model-View-Template architecture
the view is used to handle request and send back response when a url is called.
To increase the cohesion of the view component, a service layer has also been created.
So the view will often call functions in the service layer for complex computational operations

"""
from django.shortcuts import render
from django.core.files.storage import FileSystemStorage
from . import services
from base64 import b64encode

"""
The static path for the images
"""
picture_path_SVF_original = "mainsite/static/mainsite/png/dj.png"
picture_path_SVF_cropped = "mainsite/static/mainsite/png/dj2.png"
picture_path_SAT_original = "mainsite/static/mainsite/png/dj3.png"
picture_path_SAT_cropped = "mainsite/static/mainsite/png/dj4.png"
picture_path_MOS_cubic = "mainsite/static/mainsite/png/dj5.png"
picture_path_MOS_pano = "mainsite/static/mainsite/png/dj6.png"



def index(request):
    return render(request,'mainsite/index.html')


def map(request):
    apikey = services.image_retrieval.apikey
    return render(request, 'mainsite/home.html', {'apikey': apikey})

"""
The page that retrieves coordinates from restful API and ask
the google street view API for the images at that location
"""
def search_result(request):
    lat = request.GET.get('lat', '')
    lng = request.GET.get('lng', '')
    # empty coordinates data, return error
    if lat == '' or lng == '':
        return render(request, 'mainsite/error.html', {'error_message': "No street view found"})
    coor_dict = {'lat':lat,'lng':lng}
    # try to added the coordinates as a new grid to the databse, if the grid already exists
    # go to else
    if services.add_new_grid(lat,lng):
        metadata = services.get_streetview_metadata(lat,lng)
        status = metadata['status']
        # check if google api has image for that location,then retrieve and stitch those images
        if status == 'OK' and services.get_streetview_picture(lat, lng, picture_path_SVF_original):
            result_dict = {'ORI': 0, 'SAT': 0, 'SKY': 0}
            return render(request, 'mainsite/search_result.html', {**result_dict,**coor_dict})
        else:
            return render(request, 'mainsite/error.html', {'error_message': "No street view found"})
    else:
        result_dict = services.load_old_grid(lat, lng)
        return render(request, 'mainsite/search_result.html', {**result_dict,**coor_dict})

"""
the page used by the user to mark the picture
"""
def marking_svf(request):

    lat = request.POST.get('lat', '')[:-1]# getting rid of the back slashes from a restful request
    lng = request.POST.get('lng', '')[:-1]
    if lat == '' or lng == '':
        return render(request, 'mainsite/error.html', {'error_message': "No street view found"})
    coor_dict = {'lat':lat,'lng':lng}

    # check if there is an uploading file
    files_not_empty = not not request.FILES
    if request.method == 'POST' and files_not_empty and request.FILES['uploaded_image']:
        uploaded_image = request.FILES['uploaded_image']
        fs = FileSystemStorage()
        fs.delete(picture_path_SVF_original)
        fs.save(picture_path_SVF_original, uploaded_image)
        with open(picture_path_SVF_original, 'rb') as f:
            image_data = f.read()
            services.update_image(image_data,lat,lng)
    # no uploading file, load the current image
    pic_size = services.get_image_size(picture_path_SVF_original)
    return render(request, 'mainsite/marking_svf.html', {**pic_size,**coor_dict})


def SVF_report(request):
    # get the data from redirect
    percentage = request.GET.get('percentage')
    lat = request.GET.get('lat')
    lng = request.GET.get('lng')
    return render(request,'mainsite/SVFReport.html',{'percentage':percentage,'lat':lat,'lng':lng})


"""
this view is a middle view to prevent form re-submission
"""
def process_SVF_image(request):
    data_from_client = request.POST.get('hidden_image_field')
    lat = request.POST.get('lat', '')[:-1]
    lng = request.POST.get('lng', '')[:-1]
    image_data = services.process_client_image(data_from_client)
    if image_data is None:
        return render(request, 'mainsite/error.html', {'error_message': "you didn't crop the image"})
    percentage = services.generate_SVF_data(picture_path_SVF_cropped, image_data, lat, lng)
    return services.custom_redirect('/SVFReport',lat=lat,lng=lng,percentage=percentage)

"""
the page used by the user to mark satellite image
"""
def marking_satellite(request):
    # check if there is an uploading file
    files_not_empty = not not request.FILES
    if request.method == 'POST' and files_not_empty and request.FILES['uploaded_image']:
        uploaded_image = request.FILES['uploaded_image']
        fs = FileSystemStorage()
        fs.delete(picture_path_SAT_original)
        file_name = fs.save(picture_path_SAT_original, uploaded_image)
        file_url = fs.url(file_name)

    pic_size = services.get_image_size(picture_path_SAT_original)
    return render(request, 'mainsite/marking_sat.html', pic_size)

"""
The page that displays the satellite image report
"""
def SAT_report(request):
    image_data_from_client = request.POST.get('hidden_image_field')

    grid_number = int(request.POST.get('hidden_grid_field'))

    image_data = services.process_client_image(image_data_from_client)
    if image_data is None:
        return render(request, 'mainsite/error.html', {'error_message': "you didn't mark the image"})
    # for now,we only use 3 colours
    colour_list = services.process_colour_list_string()
    colour_percentage_dict = services.generate_SAT_data(picture_path_SAT_cropped, image_data, colour_list)
    grid_dict = services.generate_grid_data(picture_path_SAT_cropped, colour_list, grid_number)

    return render(request, 'mainsite/SATReport.html', {'colour_percentage_dict':colour_percentage_dict,
                                                       'grid_dict': grid_dict ,'grid_number': grid_number})

"""
the page to display all the marked and saved pictures in a particular location
"""
def show_all_SVF(request):
    lat = request.POST.get('lat', '')[:-1]
    lng = request.POST.get('lng', '')[:-1]
    all_svf_image = services.get_all_image(lat, lng,'SKY')
    return render(request,'mainsite/show_all_SVF.html',{'all_svf_image':all_svf_image})

"""
display the selected SVF picture
"""
def display_image_SVF(request,image_id):
    result = services.find_an_image(image_id)[0] # there will only be one result
    services.load_image_as_file(result, 'SVF')

    return render(request,'mainsite/display_image_SVF.html',
                  {'image':result})

"""
display all the visited grid
"""
def display_all_grid(request):
    all_grids = services.get_all_grid()
    print(all_grids)
    return render(request,'mainsite/show_all_grid.html',{'all_grids':all_grids})


"""
maskout_sky_using meanshift
"""
def maskout_sky(request):
    lat = request.POST.get('lat', '')[:-1]
    lng = request.POST.get('lng', '')[:-1]
    services.prepare_maskout_sky_image(lat,lng)
    return render(request,'mainsite/maskout_sky.html')