from django.urls import path

from . import views

urlpatterns = [
    path('map', views.map, name='map'),
    path('', views.index, name='index'),
    path('index', views.index, name='index_duplicate'),
    path('search', views.search_result, name='search'),
    path('marking_svf', views.marking_svf, name='marking_svf'),
    path('SVFReport', views.SVF_report, name='SVFReport'),
    path('marking_satellite', views.marking_satellite, name='marking_satellite'),
    path('SATReport', views.SAT_report, name='SATReport'),
    path('show_all_SVF',views.show_all_SVF,name='show_all_SVF'),
    path('display_image/<int:image_id>/', views.display_image_SVF, name='display_image'),
    path('processSVF',views.process_SVF_image,name='processSVF'),
    path('display_all_grid',views.display_all_grid,name='display_all_grid'),
    path('maskout_sky',views.maskout_sky,name='maskout_sky')
]