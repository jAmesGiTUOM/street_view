import psycopg2
import psycopg2.extras
from mainsite.models.db_connection import conn_string


class Grid:

    @staticmethod
    def add_new_grid(x,y):
        try:
            conn = psycopg2.connect(conn_string)
            cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            cursor.execute("INSERT INTO grid (x_coordinate,y_coordinate,has_SAT_image,has_SKY_image) VALUES(%s,%s,FALSE ,FALSE)",(x,y))
            conn.commit()
            cursor.close()
            return True
        except Exception as e:
            return False

    @staticmethod
    def get_all_grid():
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        cursor.execute("SELECT * from grid")
        result = cursor.fetchall()
        cursor.close()
        conn.close()
        return result