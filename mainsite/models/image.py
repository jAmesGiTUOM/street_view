import psycopg2
import psycopg2.extras
from mainsite.models.db_connection import conn_string

class Image:

    @staticmethod
    def add_new_image(image_data, x, y, image_type, grid_breakdown, total_breakdown, SVF):
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cursor.execute("insert into image (image_id,image_data,grid_breakdown,total_breakdown,SVF,x,y,image_type)"
                       " VALUES(DEFAULT,%s,%s,%s,%s,%s,%s,%s) ",
                       (image_data, grid_breakdown, total_breakdown, SVF, x, y, image_type))
        conn.commit()
        cursor.close()
        conn.close()

    @staticmethod
    def get_all_image_data_at_a_location(x,y,type):
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        if type is None:
           cursor.execute("SELECT * from image where x=%s and y=%s",(x,y))
        else:
            cursor.execute("SELECT * from image where x=%s and y=%s and image_type = %s", (x, y, type))

        result = cursor.fetchall()
        cursor.close()
        conn.close()
        # the result is a list of dict, the image data needs to be retrieved using  "bytes(result[i]['image_data'])"
        return result

    @staticmethod
    def get_image_data_by_id(image_id):
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        cursor.execute("SELECT * from image where image_id=%s", (image_id,))
        result = cursor.fetchall()
        cursor.close()
        conn.close()
        return result

    @staticmethod
    def update_image_data_original(image_data,lat,lng):
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cursor.execute("UPDATE image SET image_data = (%s),created_at= DEFAULT WHERE image_type = 'ORI' and x = %s and y = %s",(image_data, lat, lng))
        conn.commit()
        cursor.close()
        conn.close()
