$(document).ready(function() {

    var condition = 1;
    var points = [];//holds the mousedown points
    var canvas = document.getElementById('myCanvas');
    var auto_complete = true;
    this.isOldIE = (window.G_vmlCanvasManager);
    var originalImageObj = new Image();

    $(function() {
      //  if (document.domain == 'localhost') {

            if (this.isOldIE) {
                G_vmlCanvasManager.initElement(myCanvas);
            }
            var ctx = canvas.getContext('2d');

            var imageObj = new Image();
            function init() {
                canvas.addEventListener('mousedown', mouseDown, false);
                canvas.addEventListener('mouseup', mouseUp, false);
                canvas.addEventListener('mousemove', mouseMove, false);
            }

            // Draw  image onto the canvas
            imageObj.onload = function() {
                ctx.drawImage(imageObj, 0, 0);

            };
            imageObj.src = "static/mainsite/png/dj.png?"+(new Date()).getTime();

            originalImageObj = imageObj;

            // Switch the blending mode
            ctx.globalCompositeOperation = 'destination-over';




            //mousemove event
            $('#myCanvas').mousemove(function(e) {

                    ctx.beginPath();

                    $('#posx').html(e.offsetX);
                    $('#posy').html(e.offsetY);
            });
            //mousedown event
            $('#myCanvas').mousedown(function(e) {
                    if (e.which == 1) {
                        var pointer = $('<span class="spot">').css({
                            'position': 'absolute',
                            'background-color': '#000000',
                            'width': '5px',
                            'height': '5px',
                            'top': e.pageY,
                            'left': e.pageX


                        });
                        //store the points on mousedown
                        points.push(e.pageX, e.pageY);

                        ctx.globalCompositeOperation = 'destination-out';
                        var oldposx = $('#oldposx').html();
                        var oldposy = $('#oldposy').html();
                        var posx = $('#posx').html();
                        var posy = $('#posy').html();
                        ctx.beginPath();
                        ctx.moveTo(oldposx, oldposy);
                        if(condition == 0)
                        {
                            //meaning that one crop is finished
                            oldposx = '';
                            oldposy = '';
                            condition = 1;
                        }

                        if (oldposx != '') {
                            ctx.lineTo(posx, posy);
                            ctx.stroke();
                        }
                        $('#oldposx').html(e.offsetX);
                        $('#oldposy').html(e.offsetY);
                    }
                    $(document.body).append(pointer);
                    $('#posx').html(e.offsetX);
                    $('#posy').html(e.offsetY);



            });
            $('#refresh_button').click(function () {
                ctx.drawImage(originalImageObj,0,0);
                imageObj = originalImageObj;
            });

            $('#crop').click(function() {
                condition = 0;

                $('.spot').each(function() {
                    $(this).remove();

                });
                //clear canvas
                ctx.clearRect(0, 0, imageObj.width, imageObj.height);
                ctx.beginPath();
                ctx.width = imageObj.width;
                ctx.height = imageObj.height;
                ctx.globalCompositeOperation = 'destination-over';
                //draw the polygon
                setTimeout(function() {


                    var offset = $('#myCanvas').offset();

                    if(auto_complete){

                        // insert the top right corner
                        points.push(offset.left+imageObj.width);
                        points.push(offset.top);
                        // insert the bottom right corner
                        points.push(offset.left+imageObj.width);
                        points.push(offset.top+imageObj.height);
                        // insert the bottom left corner
                        points.push(offset.left);
                        points.push(offset.top+imageObj.height);
                        // insert the top right corner
                        points.push(offset.left);
                        points.push(offset.top);

                    }

                    for (var i = 0; i < points.length; i += 2) {
                        var x = parseInt(jQuery.trim(points[i]));
                        var y = parseInt(jQuery.trim(points[i + 1]));


                        if (i == 0) {
                            ctx.moveTo(x - offset.left, y - offset.top);
                        } else {
                            ctx.lineTo(x - offset.left, y - offset.top);
                        }
                        //console.log(points[i],points[i+1])
                    }

                    if (this.isOldIE) {

                        ctx.fillStyle = '';
                        ctx.fill();
                        var fill = $('fill', myCanvas).get(0);
                        fill.color = '';
                        fill.src = element.src;
                        fill.type = 'tile';
                        fill.alignShape = false;
                    }
                    else {
                        var pattern = ctx.createPattern(imageObj, "repeat");
                        ctx.fillStyle = pattern;
                        ctx.fill();
                        points = [];
                        ctx.closePath();
                        var dataURL = canvas.toDataURL("image/png");
                        document.getElementById('hidden_image_field').value = dataURL;
                        var newImageObj = new Image();
                        newImageObj.src = dataURL;
                        ctx.drawImage(newImageObj,0,0);
                        imageObj = newImageObj;


                    }
                }, 20);


            });
            $('#auto_button').click(function() {
                var button = document.getElementById('auto_button');
                if(auto_complete == true) {
                    button.value = "AUTO_COMPLETE_OFF";
                    auto_complete = false;
                }
                else{
                    button.value = "AUTO_COMPLETE_ON";
                    auto_complete = true;
                }
            });


    });

});

