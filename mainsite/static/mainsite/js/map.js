var mymarker;
function myMap() {
var infoWindowList = []
var locationMelbourne = new google.maps.LatLng(-37.812636, 144.967965);
var mapProp= {
    center:locationMelbourne,
    zoom:15,
    clickableIcons: false //disable the clickable landmarks
};
var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

mymarker = new google.maps.Marker({
          position:locationMelbourne,
          map: map
        });

mymarker.addListener('rightclick',function(){
         mymarker.setMap(null)
     });

mymarker.addListener('click',function () {
         var coordInfoWindow = new google.maps.InfoWindow();
         coordInfoWindow.setPosition(mymarker.position);
         coordInfoWindow.setContent(clickContent());
         coordInfoWindow.open(map)
         infoWindowList.push(coordInfoWindow)
     });
map.addListener('click',function(e){
     var arrayLength = infoWindowList.length
     for(i=0;i<arrayLength;i++){
         infoWindowList[i].close()
     }
     mymarker.setMap(map)
     mymarker.setPosition(e.latLng)


})
}
function clickContent()
{
    return [
        '<a href="" onclick="redirectTo();event.preventDefault();" >click here for a street view panorama</a>'
    ].join('<br>');
}

function redirectTo(){
    window.location.href="search?lat="+mymarker.getPosition().lat()+"&lng="+mymarker.getPosition().lng()
}