
$(document).ready(function() {

    var condition = 1;
    var points = [];//holds the mousedown points
    var canvas = document.getElementById('myCanvas');
    this.isOldIE = (window.G_vmlCanvasManager);
    var originalImageObj = new Image();
    var previousImageObj = new Image();
    var hasGrid = false;
    var firstLoad = true;
    $(function() {

            if (this.isOldIE) {
                G_vmlCanvasManager.initElement(myCanvas);
            }
            var ctx = canvas.getContext('2d');

            var imageObj = new Image();
            function init() {
                canvas.addEventListener('mousedown', mouseDown, false);
                canvas.addEventListener('mouseup', mouseUp, false);
                canvas.addEventListener('mousemove', mouseMove, false);
            }

            // Draw  image onto the canvas
            imageObj.onload = function() {
                if(firstLoad)
                ctx.drawImage(imageObj, 0, 0);

            };
            //a hack to disable cache for the image object
            imageObj.src = "static/mainsite/png/dj3.png?"+(new Date()).getTime();

            originalImageObj.src = imageObj.src;

            // Switch the blending mode
            ctx.globalCompositeOperation = 'destination-over';

            //added enter key listener
            window.onkeyup = function(e) {
                var key = e.keyCode ? e.keyCode : e.which;
                if(key == 13){
                    $('#crop').click();
                }
            };

            //mousemove event
            $('#myCanvas').mousemove(function(e) {

                    ctx.beginPath();

                    $('#posx').html(e.offsetX);
                    $('#posy').html(e.offsetY);
            });
            //mousedown event
            $('#myCanvas').mousedown(function(e) {
                    if (e.which == 1) {
                        var pointer = $('<span class="spot">').css({
                            'position': 'absolute',
                            'background-color': '#000000',
                            'width': '5px',
                            'height': '5px',
                            'top': e.pageY,
                            'left': e.pageX
                        });
                        //store the points on mousedown
                        points.push(e.pageX, e.pageY);

                        ctx.globalCompositeOperation = 'destination-out';
                        var oldposx = $('#oldposx').html();
                        var oldposy = $('#oldposy').html();
                        var posx = $('#posx').html();
                        var posy = $('#posy').html();
                        ctx.beginPath();
                        ctx.moveTo(oldposx, oldposy);
                        if(condition == 0)
                        {
                            //meaning that one crop is finished
                            oldposx = '';
                            oldposy = '';
                            condition = 1;
                        }

                        if (oldposx != '') {
                            ctx.lineTo(posx, posy);
                            ctx.stroke();
                        }
                        $('#oldposx').html(e.offsetX);
                        $('#oldposy').html(e.offsetY);
                    }
                    $(document.body).append(pointer);
                    $('#posx').html(e.offsetX);
                    $('#posy').html(e.offsetY);



            });
            $('#refresh_button').click(function () {
                ctx.globalCompositeOperation = 'source-over';
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(originalImageObj,0,0);
                imageObj.src = originalImageObj.src;
                    hasGrid = false;

            });


            $('#remove_grid_button').click(function () {
                ctx.globalCompositeOperation = 'source-over';
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(imageObj,0,0);
                hasGrid = false;
            });

             $('#undo_button').click(function () {
                ctx.globalCompositeOperation = 'source-over';
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(previousImageObj,0,0);
                hasGrid = false;
                imageObj.src = previousImageObj.src;
            });


            $('#crop').click(function() {
                condition = 0;

                $('.spot').each(function() {
                    $(this).remove();

                });
                //clear canvas
                //ctx.clearRect(0, 0, imageObj.width, imageObj.height);
                ctx.beginPath();
                ctx.width = imageObj.width;
                ctx.height = imageObj.height;
                ctx.globalCompositeOperation = 'source-over';
                //draw the polygon
                setTimeout(function() {

                    var offset = $('#myCanvas').offset();


                    for (var i = 0; i < points.length; i += 2) {
                        var x = parseInt(jQuery.trim(points[i]));
                        var y = parseInt(jQuery.trim(points[i + 1]));


                        if (i == 0) {
                            ctx.moveTo(x - offset.left, y - offset.top);
                        } else {
                            ctx.lineTo(x - offset.left, y - offset.top);
                        }
                        //console.log(points[i],points[i+1])
                    }

                    if (this.isOldIE) {

                        ctx.fillStyle = '';
                        ctx.fill();
                        var fill = $('fill', myCanvas).get(0);
                        fill.color = '';
                        fill.src = element.src;
                        fill.type = 'tile';
                        fill.alignShape = false;
                    }
                    else {
                        var removed_grid = false;
                        if(hasGrid)
                        {
                            $('#remove_grid_button').click();
                            removed_grid = true;
                        }
                        previousImageObj.src = imageObj.src;
                        var colour = document.getElementById('color_selector').value;
                        ctx.fillStyle = colour;
                        ctx.fill();
                        points = [];
                        ctx.closePath();
                        var dataURL = canvas.toDataURL("image/png");
                        document.getElementById('hidden_image_field').value = dataURL;
                        firstLoad = false;
                        imageObj.src = dataURL;
                        if(removed_grid)
                        $('#grid_button').click();


                    }
                }, 20);


            });

        $('#grid_button').click(function(){

            if(hasGrid){
                return
            }
            ctx.globalCompositeOperation = 'source-over';

            var totalWidth = canvas.width;
            var totoalHeight = canvas.height;

            var numberOfGrid = parseInt(document.getElementById('grid_selector').value);


            var gridSizeX = totalWidth/numberOfGrid;
            var gridSizeY = totoalHeight/numberOfGrid;

            var lineWidth = 1;
            ctx.beginPath();

            for (var x = 0; x <= totalWidth; x += gridSizeX) {
                ctx.moveTo(0.5 + x , 0);
                ctx.lineTo(0.5 + x , totoalHeight );
            }


            for (var y = 0; y <= totoalHeight; y += gridSizeY) {
                ctx.moveTo(0, 0.5 + y );
                ctx.lineTo(totalWidth , 0.5 + y );
            }
            ctx.strokeStyle = "black";
            ctx.lineWidth = lineWidth;
            ctx.stroke();
            hasGrid = true;
            var dataURL = canvas.toDataURL("image/png");
            document.getElementById('hidden_image_field').value = dataURL;
            document.getElementById('grid_number').value = numberOfGrid;

        });


    });

});

